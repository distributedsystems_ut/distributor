import logging
logging.basicConfig(level=logging.DEBUG,\
                    format='%(asctime)s (%(threadName)-2s) %(message)s',)
from socket import AF_INET, SOCK_STREAM, socket, SHUT_WR
from socket import error as soc_err
from threading import Thread
import re

class ClientHandler(Thread):

    def __init__(self,client_socket,client_addr, balancers_file, distributor, buff_size=1024, enc = 'utf-8'):
        Thread.__init__(self)
        self.__client_socket = client_socket
        self.__client_address = client_addr
        self.__balancers_file = balancers_file
        self.__buff_size = buff_size
        self.__enc = enc
        self.__distributor = distributor

    def run(self):
        self.__handle()

    def handle(self):
        self.start()

    def __parse_message(self, m):
        n, reg, lang, x, y = re.split(r'[/,@:]', m)
        return n, reg, lang, float(x), float(y)
    def __parse_ping_message(self, m):
        n, p = re.split(r'[/]', m)
        return n, p

    def __parse_line(self, line):
        addr, x1, y1 = re.split(':|,', line)
        return addr, float(x1), float(y1)

    def __distance(self, x, y, x1, y1):
        d = ((x - x1) ** 2 + (y - y1) ** 2) ** 0.5
        return d

    def __find_balancer(self, x, y, file_name):
        f = open(file_name, 'r')
        line = f.readline()
        addr, x1, y1 = self.__parse_line(line)
        min_dist = self.__distance(x, y, x1, y1)

        while True:
            line = f.readline()
            if not line:
                break
            new_addr, x1, y1 = self.__parse_line(line)
            dist = self.__distance(x, y, x1, y1)
            if dist < min_dist:
                addr = new_addr
                min_dist = dist
        f.close()
        return addr

    def __handle(self):
        try:
            message = self.__client_socket.recv(self.__buff_size)
            umessage = message.decode(self.__enc)
            logging.info('Received message: %s' % umessage)

            if not message or message == '':
                self.__client_socket.close()
                self.__distributor.handlers.remove(self)
                self.__distributor = None

                logging.info('Client %s:%d disconnected' % self.__client_address)
            else:
                if umessage[-1] == 'p':
                    n, p = self.__parse_ping_message(umessage)
                    self.__client_socket.sendall(p.encode(self.__enc))
                    self.__client_socket.shutdown(SHUT_WR)
                    logging.info('Sent message: %s' % p)

                else:
                    n, reg, lang, x, y = self.__parse_message(umessage)
                    balancer_addr = self.__find_balancer(x, y, self.__balancers_file)
                    self.__client_socket.sendall((reg+':'+balancer_addr).encode(self.__enc))
                    self.__client_socket.shutdown(SHUT_WR)
                    logging.info('Sent message: %s' % (reg+':'+balancer_addr))
        except soc_err as e:
            if e.errno == 107:
                logging.warn( 'Client %s:%d left before server could handle it'\
                              '' %  self.__client_address )

            else:
                logging.error( 'Error: %s' % str(e) )

class Distributor():

    def __init__(self,balancers_file,buff_size = 1024, encoding = 'utf-8'):
        self.__balancers_file = balancers_file
        self.__buff_size = buff_size
        self.__encoding = encoding
        self.handlers = []

    def listen(self,sock_addr,backlog=1):
        self.__sock_addr = sock_addr
        self.__backlog = backlog
        self.__s = socket(AF_INET, SOCK_STREAM)
        self.__s.bind(self.__sock_addr)
        self.__s.listen(self.__backlog)
        logging.debug( 'Socket %s:%d is in listening state'\
                       '' % self.__s.getsockname() )

    def loop(self):
        logging.info( 'Falling to serving loop, press Ctrl+C to terminate ...' )
        try:
            while 1:
                client_socket = None
                logging.info( 'Awaiting new clients ...' )
                client_socket,client_addr = self.__s.accept()
                logging.info('New client connected from %s:%d'\
                             '' % client_addr)
                logging.info('to port %d' '' \
                             % self.__s.getsockname()[1])
                logging.info('Service offered, client %s:%d is using a service ...' \
                             '' % client_addr)

                c = ClientHandler(client_socket,client_addr, self.__balancers_file, self,
                                  self.__buff_size, self.__encoding)
                self.handlers.append(c)
                c.handle()
        except KeyboardInterrupt:
            logging.warn( 'Ctrl+C issued closing server ...' )
      #  finally:
      #      if client_socket != None:
      #          client_socket.close()
      #          logging.info('Client %s:%d disconnected' % client_address)
        map(lambda x: x.join(),handlers)

def start_distributor(file_path, address):
    d = Distributor(file_path)
    d.listen(address)
    d.loop()


if __name__ == '__main__':
    logging.info( 'Application started' )
    t = Thread(target=start_distributor,args=('data.txt', ('', 7788)))
    t.start()
    t = Thread(target=start_distributor, args=('data.txt', ('', 7789)))
    t.start()
    logging.info ( 'Terminating ...' )