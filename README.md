##### To create a distributor: 
```python  
d = Distributor('data.txt')
d.listen(('',7779))
d.loop()
```
#### API:
* Balancer request

    * Request

      command: b (stands for 'balancer')  

      data: <language/region>@<latitude>,<longitude>

    * Responce 

      type: b

      data: <balancer_ip>@<balancer_port>

    * Example

      request: 1/92|b:UA@321.32221,90.01321

      response: 92|b:132.223.11.2@3723

* Ping

    * Request

      command: p

    * Responce 

      type: p

    * Example

      request: 1/352|p

      response: 352|p
